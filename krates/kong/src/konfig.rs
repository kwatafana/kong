//! 🎛️ `kong` server configuration

use crate::defaults;
use crate::error::KError;
use serde::Deserialize;
use std::{env, fs};

/// 🎛️ Server configuration
#[derive(Deserialize)]
pub struct Konfig {
    /// Port to access the server
    pub port: u16,
    /// Admin email address
    pub admin_email: Option<String>,
    /// Kong server working directory, path should end with `/`
    /// __defaults to kong/__
    pub working_directory: Option<String>,
    /// Name of the authorization session cookie id
    pub auth_cookie_name: String,
    /// Path to static files, __if not provided no static files will be served__
    pub static_files_path: Option<String>,
    /// Node hostname
    pub hostname: String,
    /// Kong secret key
    pub secret_key: String,
    /// Weather the server should log information to console.
    /// Console logging is __enabled__ by default.
    pub console_log: Option<bool>,
    /// Weather the server should log information to file, the file is
    /// stored in the working directory as LOG.
    /// Logging to the LOG file is __disabled__ by default
    pub log_file: Option<bool>,
}

impl Konfig {
    /// Read server environment variables
    pub fn read() -> Result<Konfig, KError> {
        let port: u16 = env::var("KONG_PORT")
            .expect("kong port not set")
            .parse()
            .expect("Invalid port");
        let hostname = env::var("KONG_HOSTNAME").expect("kong hostname not set");
        let secret_key = env::var("KONG_SECRET_KEY").expect("kong secret key not set");
        let auth_cookie_name =
            env::var("KONG_AUTH_COOKIE_NAME").expect("kong auth cookie name not set");
        let admin_email = if let Ok(email) = env::var("KONG_ADMIN_EMAIL") {
            Some(email)
        } else {
            None
        };
        let working_directory = if let Ok(dir) = env::var("KONG_WORKING_DIRECTORY") {
            Some(dir)
        } else {
            None
        };
        let static_files_path = if let Ok(path) = env::var("KONG_STATIC_FILES_PATH") {
            Some(path)
        } else {
            None
        };

        let console_log = if let Ok(console) = env::var("KONG_CONSOLE_LOG") {
            match console.as_ref() {
                "true" => Some(true),
                "false" => Some(false),
                _ => panic!("Invalid KONG_CONSOLE_LOG environment variable"),
            }
        } else {
            None
        };

        let log_file = if let Ok(file) = env::var("KONG_LOG_FILE") {
            match file.as_ref() {
                "true" => Some(true),
                "false" => Some(false),
                _ => panic!("Invalid KONG_LOG_FILE environment variable"),
            }
        } else {
            None
        };

        Ok(Konfig {
            port,
            admin_email,
            working_directory,
            auth_cookie_name,
            static_files_path,
            hostname,
            secret_key,
            console_log,
            log_file,
        })
    }

    /// Read server config file from path provided as an argument when
    /// the program was started.
    pub fn read_conf_file() -> Result<Konfig, KError> {
        let arg = env::args().nth(1);
        match arg {
            Some(a) => {
                let toml_str = fs::read_to_string(a).map_err(|_| KError::Config)?;
                let config: Konfig = toml::from_str(&toml_str).map_err(|_| KError::Config)?;
                Ok(config)
            }
            None => panic!("Path to config file is not provided!"),
        }
    }

    /// read port from config file
    pub fn read_port() -> u16 {
        let port: u16 = env::var("KONG_PORT")
            .expect("kong port not set")
            .parse()
            .expect("Invalid port");
        port
    }

    /// Read working directory from config file
    pub fn read_working_dir() -> String {
        let working_directory = if let Ok(dir) = env::var("KONG_WORKING_DIRECTORY") {
            dir
        } else {
            defaults::WORKING_DIRECTORY.to_string()
        };
        working_directory
    }

    /// read loggin
    pub fn read_logging() -> (Option<bool>, Option<bool>) {
        let console_log = if let Ok(console) = env::var("KONG_CONSOLE_LOG") {
            match console.as_ref() {
                "true" => Some(true),
                "false" => Some(false),
                _ => panic!("Invalid KONG_CONSOLE_LOG environment variable"),
            }
        } else {
            None
        };

        let log_file = if let Ok(file) = env::var("KONG_LOG_FILE") {
            match file.as_ref() {
                "true" => Some(true),
                "false" => Some(false),
                _ => panic!("Invalid KONG_LOG_FILE environment variable"),
            }
        } else {
            None
        };

        (console_log, log_file)
    }

    /// read hostname
    pub fn read_hostname() -> String {
        env::var("KONG_HOSTNAME").expect("kong hostname not set")
    }
}
